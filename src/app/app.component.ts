import {Component, OnDestroy, OnInit} from '@angular/core';
import { combineLatest, Observable, Subject, Subscription } from 'rxjs';
import { ApiData } from './services/apidata.model';
import { City } from './services/city';
import { CityService } from './services/cityService';
import { ApiDataParams } from './services/apidataparams.model';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{

    constructor() { }
  ngOnDestroy(): void {
    throw new Error('Method not implemented.');
  }

    ngOnInit() {
    }

}

