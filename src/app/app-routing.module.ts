import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { CityHistoryComponentComponent } from './city-history-component/city-history-component.component';
import { CityListComponentComponent } from './city-list-component/city-list-component.component';

const routes: Routes = [
 
  {
    path: "",
    component: CityListComponentComponent
    
  }, {
    path: "innerPage",
    component: CityHistoryComponentComponent,
  },
  {
    path: "app",
    component: CityListComponentComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule { }