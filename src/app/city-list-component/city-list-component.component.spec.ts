import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CityListComponentComponent } from './city-list-component.component';

describe('CityListComponentComponent', () => {
  let component: CityListComponentComponent;
  let fixture: ComponentFixture<CityListComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CityListComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CityListComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
