import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { ApiData } from '../services/apidata.model';
import { City } from '../services/city';
import { CityService } from '../services/cityService';

@Component({
  selector: 'app-city-list-component',
  templateUrl: './city-list-component.component.html',
  styleUrls: ['./city-list-component.component.scss']
})
export class CityListComponentComponent implements OnInit {
  title = 'weather';
  city: City[] = [];
  observablesArray=[] as  any;
    cols: any[] = [];
    observableSourceEntityData!: Observable<ApiData>
  combined: any;
  observablesResolutionSubscription!: Subscription;
    constructor(private cityService: CityService, private router: Router) { }
  ngOnDestroy(): void {
    
  }

    ngOnInit() {
       

        this.cols = [
            { field: 'name', header: 'City Name' },
            { field: 'temperature', header: 'Temperature(Celsius)' },
            { field: 'sunriseTime', header: 'Sunrise Time(24 hours)' },
            { field: 'sunsetTime', header: 'Sunset Time(24 hours)' }
        ];
        var params ;
        var cities = ["Split", "Florence", "Paris", "Barcelona", "Helsinki"];
        for(var ind=0; ind < cities.length;ind++){
        this.observableSourceEntityData = this.cityService.getCityData(cities[ind] ,params);
        this.observablesArray.push(this.observableSourceEntityData);
        }

        if (this.observablesArray != undefined && this.observablesArray.length > 0) {
          this.combined = combineLatest(this.observablesArray);
        
        } 
        if (this.combined != undefined) {
          this.observablesResolutionSubscription = this.combined.subscribe((value: string | any[]) => {
           for(var ind=0;ind < value.length;ind++){
            this.city[ind]=new Object;
             this.city[ind]["name"] = value[ind].name;
             this.city[ind]["temperature"] = value[ind].main.temp;
             this.city[ind]["sunriseTime"] = this.getHRTime(value[ind].sys.sunrise);
             this.city[ind]["sunsetTime"] = this.getHRTime(value[ind].sys.sunset);
             this.city[ind]["lat"]=value[ind].coord.lat;
             this.city[ind]["lon"]=value[ind].coord.lon;
           }
          });
    
        }
    }

    ngAfterViewInit() {

     
    }

    drillToPage(event: any, rowData: any){
      console.log(event+ rowData);
      event.preventDefault();
      const params = {lat : rowData["lat"] ,lon:rowData["lon"], cityName:rowData["name"]};
      this.router.navigate(["innerPage"],{queryParams:params});
    }

    getHRTime(time: any){
      var myDate = new Date( time *1000);
      var myDate_String = myDate.getHours()+":"+(myDate.getMinutes().toString().length > 1? myDate.getMinutes(): "0"+myDate.getMinutes())+":"+(myDate.getSeconds().toString().length > 1? myDate.getSeconds(): "0"+myDate.getSeconds());
      return myDate_String;
    }

}
