import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CityService } from './services/cityService';
import {TableModule} from 'primeng/table';
import { CityHistoryComponentComponent } from './city-history-component/city-history-component.component';
import { CityListComponentComponent } from './city-list-component/city-list-component.component';

@NgModule({
  declarations: [
    AppComponent,
    CityHistoryComponentComponent,
    CityListComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TableModule,
    HttpClientModule
  ],
  providers: [CityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
