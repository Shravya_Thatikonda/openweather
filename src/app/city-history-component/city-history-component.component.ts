import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { ApiData } from '../services/apidata.model';
import { City, CityInner } from '../services/city';
import { CityService } from '../services/cityService';

@Component({
  selector: 'app-city-history-component',
  templateUrl: './city-history-component.component.html',
  styleUrls: ['./city-history-component.component.scss']
})
export class CityHistoryComponentComponent implements OnInit {

  title = 'CityDetails';
  observablesArray = [] as any;
  cols: any[] = [];
  city: CityInner[] = [];
  observableSourceEntityData!: Observable<ApiData>
  combined: any;
  cityName:any;
  observablesResolutionSubscription!: Subscription;
  constructor(private cityService: CityService, private router: Router,
    private route: ActivatedRoute) { }
  ngOnDestroy(): void {

  }

  ngOnInit() {


    this.cols = [
      { field: 'temperature', header: 'Temperature(Celsius)' },
      { field: 'seaLevel', header: 'Sea Level' }
    ];
    var previous5DaysArray = this.Last5Days();
    this.route.queryParams
      .subscribe((params) => {
        this.cityName = params.cityName;
        for (let dayIndex = 0; dayIndex < previous5DaysArray.length; dayIndex++) {
          this.observableSourceEntityData = this.cityService.getSingeCityHistoricData(params.lat, params.lon, previous5DaysArray[dayIndex]);
          this.observablesArray.push(this.observableSourceEntityData);
        }
        if (this.observablesArray != undefined && this.observablesArray.length > 0) {
          this.combined = combineLatest(this.observablesArray);

        }
        this.observablesResolutionSubscription = this.combined.subscribe((value: string | any[]) => {
          for (var ind = 0; ind < value.length; ind++) {
            this.city[ind] = new Object;
            this.city[ind]["temperature"] = value[ind].current.temp;
            this.city[ind]["seaLevel"] = value[ind].current.pressure;
          }
        });

      });


  }

  ngAfterViewInit() {


  }
  Last5Days() {
    var result = [];
    for (var i = 0; i < 5; i++) {
      var d = new Date();
      d.setDate(d.getDate() - i);
      d.setHours(9, 0, 0, 0);
      result.push((d.getTime() - d.getMilliseconds()) / 1000);
    }
    return result;

  }

}
