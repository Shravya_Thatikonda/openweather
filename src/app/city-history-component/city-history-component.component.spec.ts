import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CityHistoryComponentComponent } from './city-history-component.component';

describe('CityHistoryComponentComponent', () => {
  let component: CityHistoryComponentComponent;
  let fixture: ComponentFixture<CityHistoryComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CityHistoryComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CityHistoryComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
