import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiData } from './apidata.model';
import { ApiDataParams } from './apidataparams.model';


@Injectable({
    providedIn: "root",
  })
export class CityService {
    constructor(private http: HttpClient) { }
  // http://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=3d8b309701a13f65b660fa2c64cdc517
   getCityData(cityName: string, params?: ApiDataParams): Observable<any> {
    const url = `http://api.openweathermap.org/data/2.5/weather?q=`+cityName+`&units=metric&appid=7bbf94f0f43046718f6ba864b61e50c7&cnt=3`;
    return this.http.post<ApiData>(url, params);
  }
  getSingeCityHistoricData(lat:string, long:string,time: any, params?: ApiDataParams){
    const url = `https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=`+lat+`&lon=`+long+`&dt=`+time+`&units=metric&appid=7bbf94f0f43046718f6ba864b61e50c7`;
    return this.http.post<ApiData>(url, params);
  }
   
}