export interface City {
    name?:string;
    temperature?:string;
    sunriseTime?:string;
    sunsetTime?:string;
    lat?:string;
    lon?:string
}

export interface CityInner{
    temperature?: string;
    seaLevel?: string;
}