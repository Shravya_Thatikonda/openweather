/**
 * Represents the Server Response for Report Data
 *
 * @author Shravya Thatikonda
 */
 export interface ApiDataParams {
    cityName: string,
    timezone: number,
    id: number,
    name: string,
    cod: number
}