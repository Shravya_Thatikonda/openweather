import { Injectable } from "@angular/core";
import {
  Event,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
  ActivatedRoute
} from "@angular/router";
import { filter } from "rxjs/operators";

/**
 * NavigationService used to navigate the application to specific pages
 *
 * @author Ashfaque Ahmad Khan
 * @since 01/22/2021
 */
@Injectable({
  providedIn: "root",
})
export class NavigationService {
  private history = [];
  private queryParams: any;
    nextRoute: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) {

  

    this.route.queryParams
      .subscribe((params) => {
        this.setQueryParams(params);
      });
  }

  /**
   * Sets the params to queryParams object
   *
   * @param params
   */
  setQueryParams(params: any) {
    this.queryParams = params;
  }

  /**
   * get QueryParams
   */
  getQueryParams() {
    return this.queryParams;
  }

  /**
   * Gets the current active route
   * @returns 
   */
  getCurrentRoute(): string {
    return this.router.url;
  }

  /**
   * Gets the next route to be activated if the navigation is successful
   * @returns 
   */
  getNextRoute(): string {
    return this.nextRoute;
  }

  /**
   * Navigates to a route with @params passed
   *
   * @param path
   * @param params
   */
  navigate(path: string) {
  
  }

  /**
   * Navigates to a route with @params passed
   *
   * @param path
   * @param params
   */
  getHostName() {
    return window.location.hostname;
  }


  public getPreviousUrl(): string {
    return this.history[this.history.length - 2] || '/home';
  }
}
