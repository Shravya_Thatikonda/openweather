Open Weather-Mobiquity The application lists down 5 European Cities and shows the temperature, sunrise time and sunset time. 
On clicking on the cityName, user can check the temperature and sea level of that single city.

Test cases, Project Breakdown, Project UI screens Link to google sheet containing the project breakdown, project UI screens and 
test cases: https://docs.google.com/spreadsheets/d/1pKq5kl_saOW1TpvLwd5HifQc5mg9IKhdP4qgFe3BkpU/edit#gid=0

Clone a repository from master branch Use these steps to clone from SourceTree, our client for using the repository 
command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, download and install first. 
If you prefer to clone from the command line, see Clone a repository.

You’ll see the clone button under the Source heading. Click that button. Now click Check out in SourceTree. 
You may need to create a SourceTree account or log in. When you see the Clone New dialog in SourceTree, 
update the destination path and name if you’d like to and then click Clone. Open the directory you just created to see your repository’s files. 
Run npm install and ng serve to get started #Plugins Used NGPrime components and icons are used to achieve the functionality